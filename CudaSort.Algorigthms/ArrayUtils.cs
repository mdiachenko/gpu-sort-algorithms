﻿using System.Text;

namespace CudaSort.Algorithms
{
    public static class ArrayUtils
    { 
        public static void RandomizeFloatArray(float[] array)
        {
            Random rand = new();

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = (float)rand.NextDouble();
            }
        }

        public static void RandomizeIntArray(int[] array, int from, int to)
        {
            Random rand = new();

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(from, to);
            }
        }

        public static string ArrayContentsToString<T>(T[] array, string separator)
        {
            StringBuilder builder = new();

            foreach(var item in array)
            {
                builder.Append(item.ToString() + separator);
            }

            return builder.ToString();
        } 

        public static void Swap<T>(ref T left, ref T right) => (left, right) = (right, left);

        public static IComparer<T> ComparerAsc<T>() where T : IComparable
            => Comparer<T>.Create((a, b) => a.CompareTo(b));

        public static IComparer<T> ComparerDesc<T>() where T : IComparable
            => Comparer<T>.Create((a, b) => b.CompareTo(a));
    }
}

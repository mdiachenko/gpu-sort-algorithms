using ManagedCuda;
using ManagedCuda.BasicTypes;
using System.Diagnostics;

namespace CudaSort.Algorithms
{
    public static class QuickSortExtension
    {
        public static void QuickSort<T>(T[] arr, int left, int right, IComparer<T> comparer)
        {
            if (left < right)
            {
                int pivotIndex = Partition(arr, left, right, comparer);
                QuickSort(arr, left, pivotIndex - 1, comparer);
                QuickSort(arr, pivotIndex + 1, right, comparer);
            }
        }

        static int Partition<T>(T[] arr, int left, int right, IComparer<T> comparer)
        {
            T pivot = arr[right];
            int i = left - 1;
            for (int j = left; j <= right - 1; j++)
            {
                if (comparer.Compare(arr[j], pivot) < 0)
                {
                    i++;
                    ArrayUtils.Swap(ref arr[i], ref arr[j]);
                }
            }
            ArrayUtils.Swap(ref arr[i + 1], ref arr[right]);
            return i + 1;
        }

        public static long QuickSort<T>(this T[] arr, bool ascending = true)
            where T: struct, IComparable
        {
            var comparer = ascending 
                ? ArrayUtils.ComparerAsc<T>() 
                : ArrayUtils.ComparerDesc<T>();

            var stopwatch = Stopwatch.StartNew();
            QuickSort(arr, 0, arr.Length - 1, comparer);
            stopwatch.Stop();

            return stopwatch.ElapsedMilliseconds;
        }

        public static long QuickSortCUDA<T>(this T[] array, bool ascending = true)
            where T: struct, IComparable
        {
            PrimaryContext ctx = new(0);
            var driverVersion = ctx.GetDeviceInfo().DriverVersion;
            CudaDeviceVariable<float> dev_mem = new(array.Length);

            dev_mem.CopyToDevice(array);

            CudaLinker linker = new CudaLinker();
            linker.AddFile("quick_sort_kernel.ptx", CUJITInputType.PTX, null);
            linker.AddFile(@$"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v{driverVersion}\lib\x64\cudadevrt.lib", CUJITInputType.Library, null);
            byte[] image = linker.Complete();
            linker.Dispose();

            var sortKernel = ctx.LoadKernelPTX(image, "quick_sort_kernel");
            sortKernel.GridDimensions = 1;
            sortKernel.BlockDimensions = 1;

            var stopWatch = Stopwatch.StartNew();

            sortKernel.Run(dev_mem.DevicePointer, 0, array.Length - 1, 0, ascending ? 1 : 0);

            ctx.Synchronize();

            stopWatch.Stop();

            dev_mem.CopyToHost(array);

            dev_mem.Dispose();
            ctx.Dispose();

            return stopWatch.ElapsedMilliseconds;
        }
    }
}

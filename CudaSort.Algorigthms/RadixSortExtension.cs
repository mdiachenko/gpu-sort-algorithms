using ManagedCuda;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CudaSort.Algorithms
{
    public static class RadixSortExtension
    {
        public static long RadixSortCUDA<T>(this T[] array, bool ascending = true)
            where T : struct, IComparable
        {
            var ctx = new PrimaryContext(0);
            var dev_arr = new CudaDeviceVariable<T>(array.Length);
            dev_arr.CopyToDevice(array);

            ctx.SetLimit(ManagedCuda.BasicTypes.CULimit.MallocHeapSize, 2048L * 1024 * 1024);

            var kernelName = typeof(T) == typeof(int)
                ? "radix_sort_kernel_int32"
                : "radix_sort_kernel_fp32";

            var kernel = ctx.LoadKernel("radix_sort_kernel.ptx", kernelName);
            kernel.GridDimensions = 1;
            kernel.BlockDimensions = 1;

            var stopwatch = Stopwatch.StartNew();

            kernel.Run(dev_arr.DevicePointer, array.Length, ascending ? 1 : 0);
            ctx.Synchronize();

            stopwatch.Stop();

            dev_arr.CopyToHost(array);

            dev_arr.Dispose();
            ctx.Dispose();

            return stopwatch.ElapsedMilliseconds;
        }

        public static long RadixSort<T>(this T[] arr, bool ascending = true)
        {
            var stopwatch = Stopwatch.StartNew();

            if (arr == null || arr.Length < 2)
            {
                return 0;
            }

            // Determine the number of bytes required to represent each element of the array
            int bytesPerElement = Marshal.SizeOf(typeof(T));

            // Create an array of 256 buckets (i.e., count array) to hold the frequency of each byte value
            int[] count = new int[256];

            // Create an array to hold the sorted output
            T[] output = new T[arr.Length];

            // Iterate through each byte position (starting from the least significant byte) and sort the elements based on that byte
            for (int byteIndex = 0; byteIndex < bytesPerElement; byteIndex++)
            {
                // Reset the count array for each byte position
                Array.Clear(count, 0, count.Length);

                // Calculate the frequency of each byte value
                foreach (T element in arr)
                {
                    int byteValue = GetByteValue(element, byteIndex);
                    count[byteValue]++;
                }

                // Calculate the starting index of each byte value in the output array
                for (int i = 1; i < count.Length; i++)
                {
                    count[i] += count[i - 1];
                }

                // Build the output array by placing each element in its correct position
                for (int i = arr.Length - 1; i >= 0; i--)
                {
                    int byteValue = GetByteValue(arr[i], byteIndex);
                    output[--count[byteValue]] = arr[i];
                }

                // Copy the sorted output back to the input array
                Array.Copy(output, arr, arr.Length);
            }

            if (!ascending)
            {
                Array.Reverse(arr);
            }

            stopwatch.Stop();

            return stopwatch.ElapsedMilliseconds;
        }

        private static byte GetByteValue<T>(T element, int byteIndex)
        {
            if (typeof(T) == typeof(int))
            {
                int value = (int)(object)element;
                byte[] bytes = new byte[4];
                bytes[0] = (byte)(value & 0xFF);
                bytes[1] = (byte)((value >> 8) & 0xFF);
                bytes[2] = (byte)((value >> 16) & 0xFF);
                bytes[3] = (byte)((value >> 24) & 0xFF);
                return bytes[byteIndex];
            }
            else if (typeof(T) == typeof(float))
            {
                float value = (float)(object)element;
                byte[] bytes = BitConverter.GetBytes(value);
                if (byteIndex >= bytes.Length)
                {
                    return 0;
                }
                return bytes[byteIndex];
            }
            else
            {
                throw new InvalidOperationException("Radix sort is not supported for this type.");
            }
        }
    }
}

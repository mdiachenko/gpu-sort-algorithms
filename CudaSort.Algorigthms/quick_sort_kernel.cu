extern "C"
{
#define MAX_DEPTH 16
#define INSERTION_SORT 32

  ////////////////////////////////////////////////////////////////////////////////
  // Selection sort used when depth gets too big or the number of elements drops
  // below a threshold.
  ////////////////////////////////////////////////////////////////////////////////
  __device__ void selection_sort(unsigned int *data, int left, int right, bool ascending = true)
  {
    for (int i = left; i <= right; ++i)
    {
      unsigned min_val = data[i];
      int min_idx = i;

      // Find the smallest value in the range [left, right].
      for (int j = i + 1; j <= right; ++j)
      {
        unsigned val_j = data[j];

        if ((ascending && val_j < min_val) || (!ascending && val_j > min_val))
        {
          min_idx = j;
          min_val = val_j;
        }
      }

      // Swap the values.
      if (i != min_idx)
      {
        data[min_idx] = data[i];
        data[i] = min_val;
      }
    }
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Very basic quicksort algorithm, recursively launching the next level.
  ////////////////////////////////////////////////////////////////////////////////
  __global__ void quick_sort_kernel(unsigned int *data, int left, int right, int depth, bool ascending = true)
  {
    // If we're too deep or there are few elements left, we use an insertion sort...
    if (depth >= MAX_DEPTH || right - left <= INSERTION_SORT)
    {
      selection_sort(data, left, right, ascending);
      return;
    }

    unsigned int *lptr = data + left;
    unsigned int *rptr = data + right;
    unsigned int pivot = data[(left + right) / 2];

    // Do the partitioning.
    while (lptr <= rptr)
    {
      // Find the next left- and right-hand values to swap
      unsigned int lval = *lptr;
      unsigned int rval = *rptr;

      // Move the left pointer as long as the pointed element is smaller than the pivot.
      while ((ascending && lval < pivot) || (!ascending && lval > pivot))
      {
        lptr++;
        lval = *lptr;
      }

      // Move the right pointer as long as the pointed element is larger than the pivot.
      while ((ascending && rval > pivot) || (!ascending && rval < pivot))
      {
        rptr--;
        rval = *rptr;
      }

      // If the swap points are valid, do the swap!
      if (lptr <= rptr)
      {
        *lptr++ = rval;
        *rptr-- = lval;
      }
    }

    // Now the recursive part
    int nright = rptr - data;
    int nleft = lptr - data;

    // Launch a new block to sort the left part.
    if (left < (rptr - data))
    {
      cudaStream_t s;
      cudaStreamCreateWithFlags(&s, cudaStreamNonBlocking);
      quick_sort_kernel<<<1, 1, 0, s>>>(data, left, nright, depth + 1, ascending);
      cudaStreamDestroy(s);
    }

    // Launch a new block to sort the right part.
    if ((lptr - data) < right)
    {
      cudaStream_t s1;
      cudaStreamCreateWithFlags(&s1, cudaStreamNonBlocking);
      quick_sort_kernel<<<1, 1, 0, s1>>>(data, nleft, right, depth + 1, ascending);
      cudaStreamDestroy(s1);
    }
  }
}

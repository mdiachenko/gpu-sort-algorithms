﻿using ManagedCuda;
using System.Diagnostics;

namespace CudaSort.Algorithms
{
    public static class BitonicSortExtension
    {
        static void CompareAndSwap<T>(T[] array, int i, int j, int direction)
            where T : struct, IComparable
        { 
            if (direction == (array[i].CompareTo(array[j]) > 0 ? 1 : 0))
            {
                ArrayUtils.Swap(ref array[i], ref array[j]);
            }
        }

        static void BitonicMerge<T>(T[] array, int low, int count, int direction)
            where T : struct, IComparable
        {
            if (count > 1)
            {
                int k = count / 2;
                for (int i = low; i < low + k; i++)
                {
                    CompareAndSwap(array, i, i + k, direction);
                }
                BitonicMerge(array, low, k, direction);
                BitonicMerge(array, low + k, k, direction);
            }
        }

        static void BitonicSort<T>(T[] array, int low, int count, int direction)
            where  T: struct, IComparable
        {
            if (count > 1)
            {
                int k = count / 2;

                // sort left side in ascending order
                BitonicSort(array, low, k, 1);

                // sort right side in descending order
                BitonicSort(array, low + k, k, 0);

                //Merge entire sequence in ascending order
                BitonicMerge(array, low, count, direction);
            }
        }

        public static long BitonicSort<T>(this T[] array, bool ascending = true)
            where T : struct, IComparable
        {
            var stopwatch = Stopwatch.StartNew();
            BitonicSort(array, 0, array.Length, ascending ? 1 : 0);
            stopwatch.Stop();

            return stopwatch.ElapsedMilliseconds;
        }

        public static long BitonicSortCUDA<T>(this T[] array, bool ascending = true)
            where T : struct, IComparable
        {
            PrimaryContext ctx = new(0);
            CudaDeviceVariable<T> dev_arr = new(array.Length);

            ctx.SetLimit(ManagedCuda.BasicTypes.CULimit.MallocHeapSize, 256 * 1024 * 1024);

            dev_arr.CopyToDevice(array);

            var kernel = ctx.LoadKernel("bitonic_sort_kernel.ptx", "bitonic_sort_step_kernel");
            kernel.GridDimensions = array.Length / 512;
            kernel.BlockDimensions = 512;

            int j, k;

            var stopwatch = Stopwatch.StartNew();

            for (k = 2; k <= array.Length; k <<= 1)
            {
                /* Minor step */
                for (j = k >> 1; j > 0; j = j >> 1)
                {
                    kernel.Run(dev_arr.DevicePointer, j, k, ascending ? 1 : 0);
                }
            }

            ctx.Synchronize();

            stopwatch.Stop();

            dev_arr.CopyToHost(array);

            dev_arr.Dispose();
            ctx.Dispose();

            return stopwatch.ElapsedMilliseconds;
        }
    }
}

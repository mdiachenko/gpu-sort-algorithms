extern "C"
{
  __device__ void swap(int *x, int leftIdx, int rightIdx)
  {
    int tmp = x[leftIdx];
    x[leftIdx] = x[rightIdx];
    x[rightIdx] = tmp;
  }

  __device__ bool gt(int *x, int leftIdx, int rightIdx, bool ascending = true)
  {
    return (ascending && x[leftIdx] > x[rightIdx]) || (!ascending && x[leftIdx] < x[rightIdx]);
  }

  __global__ void odd_even_sort_kernel(int *x, int I, int n, bool ascending = true)
  {
    int id = blockIdx.x;
    if (I == 0 && ((id * 2 + 1) < n))
    {
      if (gt(x, id * 2, id * 2 + 1, ascending))
      {
        swap(x, id * 2, id * 2 + 1);
      }
    }
    if (I == 1 && ((id * 2 + 2) < n))
    {
      if (gt(x, id * 2 + 1, id * 2 + 2, ascending))
      {
        swap(x, id * 2 + 1, id * 2 + 2);
      }
    }
  }
}

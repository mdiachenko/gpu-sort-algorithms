extern "C"
{
	__global__ void bitonic_sort_step_kernel(float *dev_values, int j, int k, bool ascending = true)
	{
		unsigned int i, ixj; /* Sorting partners: i and ixj */
		i = threadIdx.x + blockDim.x * blockIdx.x;
		ixj = i ^ j;

		/* The threads with the lowest ids sort the array. */
		if ((ixj) > i)
		{
			if ((i & k) == 0)
			{
				/* Sort ascending */
				if ((ascending && dev_values[i] > dev_values[ixj]) || (!ascending && dev_values[i] < dev_values[ixj]))
				{
					/* exchange(i,ixj); */
					float temp = dev_values[i];
					dev_values[i] = dev_values[ixj];
					dev_values[ixj] = temp;
				}
			}
			if ((i & k) != 0)
			{
				/* Sort descending */
				if ((ascending && dev_values[i] < dev_values[ixj]) || (!ascending && dev_values[i] > dev_values[ixj]))
				{
					/* exchange(i,ixj); */
					float temp = dev_values[i];
					dev_values[i] = dev_values[ixj];
					dev_values[ixj] = temp;
				}
			}
		}
	}
}

using ManagedCuda;
using System.Diagnostics;

namespace CudaSort.Algorithms
{
    public static class OddEvenSortExtension
    {
        public static long OddEvenSort<T>(this T[] arr, bool ascending = true)
            where T : struct, IComparable
        {
            var comparer = ascending 
                ? ArrayUtils.ComparerAsc<T>() 
                : ArrayUtils.ComparerDesc<T>();

            bool isSorted = false;

            var stopwatch = Stopwatch.StartNew();
            while (!isSorted)
            {
                isSorted = true;

                // Even sort
                for (int i = 0; i < arr.Length - 1; i += 2)
                {
                    if (comparer.Compare(arr[i], arr[i + 1]) > 0)
                    {
                        ArrayUtils.Swap(ref arr[i], ref arr[i + 1]);
                        isSorted = false;
                    }
                }

                // Odd sort
                for (int i = 1; i < arr.Length - 1; i += 2)
                {
                    if (comparer.Compare(arr[i], arr[i + 1]) > 0)
                    {
                        ArrayUtils.Swap(ref arr[i], ref arr[i + 1]);
                        isSorted = false;
                    }
                }
            }

            return stopwatch.ElapsedMilliseconds;
        }

        public static long OddEvenSortCUDA<T>(this T[] array, bool ascending = true)
            where T : struct
        {
            var ctx = new PrimaryContext(0);
            var dev_arr = new CudaDeviceVariable<T>(array.Length);
            dev_arr.CopyToDevice(array);

            var kernel = ctx.LoadKernel("odd_even_sort_kernel.ptx", "odd_even_sort_kernel");
            kernel.GridDimensions = array.Length / 2;
            kernel.BlockDimensions = 1;

            var stopwatch = Stopwatch.StartNew();
            
            for (int i = 0; i < array.Length; i++)
            {
                kernel.Run(dev_arr.DevicePointer, i % 2, array.Length, ascending ? 1 : 0);
            }

            ctx.Synchronize();

            stopwatch.Stop();

            dev_arr.CopyToHost(array);

            dev_arr.Dispose();
            ctx.Dispose();

            return stopwatch.ElapsedMilliseconds;
        }
    }
}

#include <thrust/device_ptr.h>
#include <thrust/sort.h>

extern "C" {
	__global__ void radix_sort_kernel_int32(int* dev_ptr, int n, bool ascending = true)
	{
			thrust::device_ptr<int> dev_begin(dev_ptr);
			thrust::device_ptr<int> dev_end(dev_ptr + n);
			if (ascending) {
				thrust::sort(thrust::device, dev_begin, dev_end, thrust::less<int>());
			} else {
				thrust::sort(thrust::device, dev_begin, dev_end, thrust::greater<int>());
			}
	}

	__global__ void radix_sort_kernel_fp32(float* dev_ptr, int n, bool ascending = true)
	{
			thrust::device_ptr<float> dev_begin(dev_ptr);
			thrust::device_ptr<float> dev_end(dev_ptr + n);
			if (ascending) {
				thrust::sort(thrust::device, dev_begin, dev_end, thrust::less<float>());
			} else {
				thrust::sort(thrust::device, dev_begin, dev_end, thrust::greater<float>());
			}
	}
}

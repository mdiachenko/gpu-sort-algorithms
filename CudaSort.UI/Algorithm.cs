﻿namespace CudaSort.UI
{
    public static class Algorithm
    {
        public const string QuickSort = "Quicksort";
        public const string OddEvenSort = "Odd-Even sort";
        public const string RadixSort = "Radix sort";
        public const string BitonicSort = "Bitonic sort";
    }
}

﻿namespace CudaSort.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            statusStrip1 = new StatusStrip();
            statusText = new ToolStripStatusLabel();
            panel1 = new Panel();
            cbSave = new CheckBox();
            cbDescending = new CheckBox();
            cbSize = new ComboBox();
            label3 = new Label();
            panel3 = new Panel();
            rbFloat = new RadioButton();
            rbInteger = new RadioButton();
            label2 = new Label();
            label1 = new Label();
            cbAlgorithm = new ComboBox();
            button1 = new Button();
            panel2 = new Panel();
            lwDetails = new ListView();
            ColName = new ColumnHeader();
            ColValue = new ColumnHeader();
            backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            statusStrip1.SuspendLayout();
            panel1.SuspendLayout();
            panel3.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { statusText });
            statusStrip1.Location = new Point(0, 428);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(800, 22);
            statusStrip1.TabIndex = 0;
            statusStrip1.Text = "statusStrip1";
            // 
            // statusText
            // 
            statusText.Name = "statusText";
            statusText.Size = new Size(99, 17);
            statusText.Text = "Nothing happens";
            // 
            // panel1
            // 
            panel1.Controls.Add(cbSave);
            panel1.Controls.Add(cbDescending);
            panel1.Controls.Add(cbSize);
            panel1.Controls.Add(label3);
            panel1.Controls.Add(panel3);
            panel1.Controls.Add(label2);
            panel1.Controls.Add(label1);
            panel1.Controls.Add(cbAlgorithm);
            panel1.Controls.Add(button1);
            panel1.Dock = DockStyle.Right;
            panel1.Location = new Point(600, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(200, 428);
            panel1.TabIndex = 1;
            // 
            // cbSave
            // 
            cbSave.AutoSize = true;
            cbSave.Location = new Point(6, 352);
            cbSave.Name = "cbSave";
            cbSave.Size = new Size(131, 19);
            cbSave.TabIndex = 9;
            cbSave.Text = "Save Results To Files";
            cbSave.UseVisualStyleBackColor = true;
            // 
            // cbDescending
            // 
            cbDescending.AutoSize = true;
            cbDescending.Location = new Point(6, 327);
            cbDescending.Name = "cbDescending";
            cbDescending.Size = new Size(121, 19);
            cbDescending.TabIndex = 8;
            cbDescending.Text = "Descending Order";
            cbDescending.UseVisualStyleBackColor = true;
            // 
            // cbSize
            // 
            cbSize.DropDownStyle = ComboBoxStyle.DropDownList;
            cbSize.FormattingEnabled = true;
            cbSize.Location = new Point(6, 151);
            cbSize.Name = "cbSize";
            cbSize.Size = new Size(182, 23);
            cbSize.TabIndex = 7;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(6, 133);
            label3.Name = "label3";
            label3.Size = new Size(58, 15);
            label3.TabIndex = 6;
            label3.Text = "Array Size";
            // 
            // panel3
            // 
            panel3.Controls.Add(rbFloat);
            panel3.Controls.Add(rbInteger);
            panel3.Location = new Point(3, 89);
            panel3.Name = "panel3";
            panel3.Size = new Size(194, 25);
            panel3.TabIndex = 5;
            // 
            // rbFloat
            // 
            rbFloat.AutoSize = true;
            rbFloat.Location = new Point(100, 3);
            rbFloat.Name = "rbFloat";
            rbFloat.Size = new Size(51, 19);
            rbFloat.TabIndex = 5;
            rbFloat.Text = "Float";
            rbFloat.UseVisualStyleBackColor = true;
            // 
            // rbInteger
            // 
            rbInteger.AutoSize = true;
            rbInteger.Checked = true;
            rbInteger.Location = new Point(3, 3);
            rbInteger.Name = "rbInteger";
            rbInteger.Size = new Size(62, 19);
            rbInteger.TabIndex = 4;
            rbInteger.TabStop = true;
            rbInteger.Text = "Integer";
            rbInteger.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(6, 71);
            label2.Name = "label2";
            label2.Size = new Size(58, 15);
            label2.TabIndex = 3;
            label2.Text = "Data Type";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(6, 9);
            label1.Name = "label1";
            label1.Size = new Size(102, 15);
            label1.TabIndex = 2;
            label1.Text = "Sorting Algorithm";
            // 
            // cbAlgorithm
            // 
            cbAlgorithm.DropDownStyle = ComboBoxStyle.DropDownList;
            cbAlgorithm.FormattingEnabled = true;
            cbAlgorithm.Location = new Point(6, 27);
            cbAlgorithm.Name = "cbAlgorithm";
            cbAlgorithm.Size = new Size(182, 23);
            cbAlgorithm.TabIndex = 1;
            // 
            // button1
            // 
            button1.Location = new Point(6, 392);
            button1.Name = "button1";
            button1.Size = new Size(191, 33);
            button1.TabIndex = 0;
            button1.Text = "Start";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // panel2
            // 
            panel2.Controls.Add(lwDetails);
            panel2.Dock = DockStyle.Fill;
            panel2.Location = new Point(0, 0);
            panel2.Name = "panel2";
            panel2.Size = new Size(600, 428);
            panel2.TabIndex = 2;
            // 
            // lwDetails
            // 
            lwDetails.Columns.AddRange(new ColumnHeader[] { ColName, ColValue });
            lwDetails.Dock = DockStyle.Fill;
            lwDetails.Location = new Point(0, 0);
            lwDetails.Name = "lwDetails";
            lwDetails.Size = new Size(600, 428);
            lwDetails.TabIndex = 0;
            lwDetails.UseCompatibleStateImageBehavior = false;
            lwDetails.View = View.Details;
            // 
            // ColName
            // 
            ColName.Text = "Name";
            ColName.Width = 200;
            // 
            // ColValue
            // 
            ColValue.Text = "Value";
            ColValue.Width = 200;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.Control;
            ClientSize = new Size(800, 450);
            Controls.Add(panel2);
            Controls.Add(panel1);
            Controls.Add(statusStrip1);
            Name = "MainForm";
            Text = "CUDA Sorting Examples";
            Load += MainForm_Load;
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            panel2.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private StatusStrip statusStrip1;
        private ToolStripStatusLabel statusText;
        private Panel panel1;
        private Panel panel2;
        private ListView lwDetails;
        private ColumnHeader ColName;
        private ColumnHeader ColValue;
        private Panel panel3;
        private RadioButton rbFloat;
        private RadioButton rbInteger;
        private Label label2;
        private Label label1;
        private ComboBox cbAlgorithm;
        private Button button1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private ComboBox cbSize;
        private Label label3;
        private CheckBox cbSave;
        private CheckBox cbDescending;
    }
}
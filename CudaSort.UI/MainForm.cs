﻿using CudaSort.Algorithms;
using ManagedCuda;
using System.Text;

namespace CudaSort.UI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void AddDeviceParam(string name, object value)
        {
            lwDetails.Items.Add(new ListViewItem(new string[] { name, value.ToString() }));
        }

        private void ShowError(string text)
        {
            MessageBox.Show(text, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            int devCount = PrimaryContext.GetDeviceCount();

            if (devCount == 0)
            {
                ShowError("Cuda is not supported on your machine.");
                Application.Exit();
                return;
            }

            SetStatus("Loading device...");
            SetDeviceProps();
            FillAlgorithmOptions();
            FillArraySizeOptions();
            SetStatus("Device is ready.");
        }

        private void SetStatus(string status)
        {
            statusText.Text = status;
        }

        private void FillAlgorithmOptions()
        {
            cbAlgorithm.Items.Add(Algorithm.BitonicSort);
            cbAlgorithm.Items.Add(Algorithm.OddEvenSort);
            cbAlgorithm.Items.Add(Algorithm.QuickSort);
            cbAlgorithm.Items.Add(Algorithm.RadixSort);
            cbAlgorithm.SelectedIndex = 0;
        }

        private void FillArraySizeOptions()
        {
            for (int i = 9; i <= 24; i++)
            {
                cbSize.Items.Add((int)Math.Pow(2, i));
            }

            cbSize.SelectedIndex = 3;
        }

        private void SetDeviceProps()
        {
            var devInfo = PrimaryContext.GetDeviceInfo(0);

            AddDeviceParam("Device Name", devInfo.DeviceName);
            AddDeviceParam("Device Id", devInfo.PciDeviceId);
            AddDeviceParam("Driver Version", devInfo.DriverVersion);
            AddDeviceParam("Max Block Dimensions", devInfo.MaxBlockDim);
            AddDeviceParam("Max Grid Dimensions", devInfo.MaxGridDim);
            AddDeviceParam("Max Shared Memory Per Block", devInfo.MaxSharedMemoryPerBlockOptin.ToString("0 bytes"));
            AddDeviceParam("Max Threads Per Block", devInfo.MaxThreadsPerBlock);
            AddDeviceParam("Compute Capability", devInfo.ComputeCapability);
            AddDeviceParam("Clock Rate", devInfo.ClockRate.ToString("0 kHz"));
            AddDeviceParam("Memory Clock Rate", devInfo.MemoryClockRate.ToString("0 kHz"));
            AddDeviceParam("Warp Size", devInfo.WarpSize.ToString("0 threads"));
            AddDeviceParam("Multiprocessor Count", devInfo.MultiProcessorCount);
            AddDeviceParam("L2 Cache Size", devInfo.L2CacheSize.ToString("0 bytes"));
            AddDeviceParam("Caching Locals in L1 supported", devInfo.LocalL1CacheSupported ? "Yes" : "No");
            AddDeviceParam("Caching Global in L1 supported", devInfo.GlobalL1CacheSupported ? "Yes" : "No");
            AddDeviceParam("Can Execute Multiple Kernels Concurrently", devInfo.ConcurrentKernels ? "Yes" : "No");
            AddDeviceParam("Can Allocate Managed Memory", devInfo.ManagedMemory ? "Yes" : "No");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int size = (int)cbSize.SelectedItem;

            if (rbFloat.Checked)
            {
                var arr = PrepareFloatArray(size);
                StartAlgorithm(arr);

            }
            else if (rbInteger.Checked)
            {
                var arr = PrepareIntArray(size);
                StartAlgorithm(arr);
            }
        }

        private float[] PrepareFloatArray(int size)
        {
            var arr = new float[size];

            ArrayUtils.RandomizeFloatArray(arr);

            return arr;
        }

        private int[] PrepareIntArray(int size)
        {
            var arr = new int[size];

            ArrayUtils.RandomizeIntArray(arr, 0, size);

            return arr;
        }

        private static void SaveArrayToFile<T>(T[] arr, string filename)
        {
            using var fs = new FileStream(filename, FileMode.Create, FileAccess.Write);
            using var writer = new StreamWriter(fs);
            writer.WriteLine(ArrayUtils.ArrayContentsToString(arr, "\n"));
        }

        private void StartAlgorithm<T>(T[] arr)
            where T : struct, IComparable
        {
            var algorithm = (string)cbAlgorithm.SelectedItem;
            var ascending = !cbDescending.Checked;
            var saveToFile = cbSave.Checked;

            long elapsedTimeHost = 0, elapsedTimeDevice = 0;

            var hostArr = new T[arr.Length];
            var deviceArr = new T[arr.Length];
            arr.CopyTo(hostArr, 0);
            arr.CopyTo(deviceArr, 0);

			try{
            switch (algorithm)
            {
                case Algorithm.RadixSort:
                    elapsedTimeHost = hostArr.RadixSort(ascending);
                    elapsedTimeDevice = deviceArr.RadixSortCUDA(ascending);
                    break;

                case Algorithm.QuickSort:
                    elapsedTimeHost = hostArr.QuickSort(ascending);
                    elapsedTimeDevice = deviceArr.QuickSortCUDA(ascending);
                    break;

                case Algorithm.OddEvenSort:
                    elapsedTimeHost = hostArr.OddEvenSort(ascending);
                    elapsedTimeDevice = deviceArr.OddEvenSortCUDA(ascending);
                    break;

                case Algorithm.BitonicSort:
                    elapsedTimeHost = hostArr.BitonicSort(ascending);
                    elapsedTimeDevice = deviceArr.BitonicSortCUDA(ascending);
                    break;

                default:
                    ShowError("Unknown algorithm");
                    break;
            }
			catch (CudaException e){
				ShowError(e.Message);
			}

            MessageBox.Show($"Elapsed time host: {elapsedTimeHost}ms\nElapsed time device: {elapsedTimeDevice}ms");

            if (saveToFile)
            {
                SetStatus("Saving results to files...");
                SaveArrayToFile(arr, "UNSORTED_array.txt");
                SaveArrayToFile(hostArr, "HOST-SORTED_array.txt");
                SaveArrayToFile(deviceArr, "DEVICE-SORTED_array.txt");
                SetStatus("Results were successfully saved to files...");
            }
        }
    }
}
